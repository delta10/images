#!/bin/sh

# Dump all databases on a server. Retain only backups within the retention period.
#
# PGHOST: The hostname of the Postgres server
# PGUSER: The user to login
# PGPASSWORD: The password of the user
# BACKUP_DIR: The directory to backup to
# RETENTION_DAYS: The retention of the backups in days

PGHOST="${PGHOST:-localhost}"
PGUSER="${PGUSER:-postgres}"
PGPASSWORD="${PGPASSWORD}"
BACKUP_DIR="${BACKUP_DIR:-/persistent-volumes/backups}"
RETENTION_DAYS=${RETENTION_DAYS:-14}

BACKUP_DATE=`date +%Y%m%d%H%M%s`

mkdir -p $BACKUP_DIR && chmod 0700 $BACKUP_DIR

DATABASES=`psql -h $PGHOST -U $PGUSER -l -t | cut -d'|' -f1 | sed -e 's/ //g' -e '/^$/d' | grep -v 'template'`
if [[ $? -ne 0 ]]; then
    echo "Could not retrieve databases."
    exit 1
fi

for DATABASE in $DATABASES; do  if [ "$DATABASE" != "postgres" ] && [ "$DATABASE" != "template0" ] && [ "$DATABASE" != "template1" ] && [ "$DATABASE" != "template_postgis" ]; then
    echo "Dumping ${DATABASE}"
    pg_dump -h $PGHOST -U $PGUSER $DATABASE | gzip > $BACKUP_DIR\/$BACKUP_DATE\_$DATABASE.sql.gz
    if [[ $? -ne 0 ]]; then
        echo "Failed dumping database ${DATABASE}"
        exit 1
    else
        echo "Dumped ${DATABASE}"
    fi

    ln -fs $BACKUP_DIR\/$BACKUP_DATE\_$DATABASE.sql.gz $LATEST_DIR\/$DATABASE.sql.gz
  fi
done

find $BACKUP_DIR -type f -prune -mtime +$RETENTION_DAYS -exec rm -f {} \;
